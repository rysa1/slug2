package main

import (
	"database/sql"

	_ "gitlab.com/rysa1/slug2"
)

func main() {
	for _, driver := range sql.Drivers() {
		println(driver)
	}
}
